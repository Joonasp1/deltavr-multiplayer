using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class VRNetworkController : MonoBehaviourPunCallbacks
{

    private void Awake()
    {
        print("TEST");
        if (!photonView.IsMine)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }

}
