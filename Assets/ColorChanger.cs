using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ColorChanger : MonoBehaviourPun
{
    public List<GameObject> changeObjects;


    private void Awake()
    {
        if (!photonView.IsMine)
        {
            gameObject.SetActive(false);
        }
    }

    [PunRPC]
    void ChangeColor()
    {
        Debug.Log("BUTTON COLOR:" + GetComponent<Image>().color);
        foreach (GameObject go in changeObjects)
        {
            Debug.Log(go.name + " COLOR BEFORE: " + go.GetComponent<Renderer>().material.color);
            go.GetComponent<Renderer>().material.color = GetComponent<Image>().color;
            Debug.Log(go.name + " COLOR: " + go.GetComponent<Renderer>().material.color);
        }
    }


    public void ChangeColorPun()
    {
        photonView.RPC("ChangeColor", RpcTarget.AllBuffered);
    }
}
