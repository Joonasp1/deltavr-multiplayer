using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TreasureHunt : MonoBehaviour
{
    public TMP_Text collectedText;

    private int totalCollected;

    private void Awake()
    {
        totalCollected = 0;
    }

    void Start()
    {
        collectedText.text = totalCollected.ToString() + "/8 objects collected";
    }

    public void addTotal()
    {
        totalCollected++;
        collectedText.text = totalCollected.ToString() + "/8 objects collected";
    }
}
