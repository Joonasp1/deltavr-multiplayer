﻿Shader "DBV/Kristo/Enviorment/RGBShader"
{
    Properties
    {
        _Color1 ("Color 1", Color) = (1, 0, 0, 1)
        _Color2 ("Color 2", Color) = (0, 1, 0, 1)
        _Color3 ("Color 3", Color) = (0, 0, 1, 1)

        _Speed ("Speed", float) = 1
        _Emission ("Emission", Range(0.5,5)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float _Speed, _Emission;
            fixed4 _Color1, _Color2, _Color3;

            fixed4 frag (v2f i) : SV_Target
            {
                float cycleTime = fmod(_Time.x * _Speed, 3);
                float amount = cycleTime - floor(cycleTime);
                fixed4 col = fixed4(0,0,0,0);

                int baseCol = floor(cycleTime);
                int topCol = fmod(baseCol + 1, 3);

                amount = 1 - amount;

                //Base col
                if(baseCol == 0) col += _Color1 * amount;
                else if (baseCol == 1) col += _Color2 * amount;
                else col += _Color3 * amount;

                amount = 1 - amount;

                //Next col
                if(topCol == 0) col += _Color1 * amount;
                else if (topCol == 1) col += _Color2 * amount;
                else col += _Color3 * amount;

                col *= _Emission;

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
