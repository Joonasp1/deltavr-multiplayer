using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerRPC : MonoBehaviourPunCallbacks
{
    public TMP_Text playerTag;

    [PunRPC]
    void ChangeText()
    {
        playerTag.text = photonView.ViewID.ToString();
    }



}
