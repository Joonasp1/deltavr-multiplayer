using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TreasureObject : MonoBehaviourPunCallbacks
{
    public TreasureHunt controller;
    public CollectionTray tray;
    public int row;
    
    [PunRPC]
    private void Collected()
    {
        //controller.addTotal();
        tray.RowShow(row);
        GetComponent<Renderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        //Destroy(gameObject);
    }

    public void CollectedPun()
    {
        photonView.RPC("Collected", RpcTarget.AllBuffered);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("MenuHand"))
        {
            CollectedPun();
        }
    }

}
