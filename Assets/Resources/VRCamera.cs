using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCamera : Photon.Pun.MonoBehaviourPunCallbacks
{
    [SerializeField] Camera cam;

    void Start()
    {
        if (!photonView.IsMine)
        {
            cam.enabled = false;
        }
    }


}
