using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PlayerText : MonoBehaviourPun
{
    public Transform lookCamera;

    private void Start()
    {
        photonView.RPC("ChangeText", RpcTarget.AllBuffered);
        GameObject[] players = GameObject.FindGameObjectsWithTag("MainCamera");
        foreach(GameObject player in players)
        {
            if (player.transform.parent.parent.gameObject.GetPhotonView().IsMine)
            {
                lookCamera = player.transform;
            }
            
        }
    }



    private void Update()
    {
        transform.LookAt(lookCamera);
    }
    
}
