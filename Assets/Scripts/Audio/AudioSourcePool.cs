using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    public class AudioSourcePool : MonoBehaviour
    {
        public static AudioSourcePool Instance;
        public AudioSource audioSourcePrefab;

        private List<AudioSource> _audioSources;

        public void Awake()
        {
            Instance = this;
            _audioSources = new List<AudioSource>();
        }

        public AudioSource GetAudioSource()
        {
            foreach (AudioSource source in _audioSources)
            {
                if (!source.isPlaying)
                {
                    return source;
                }
            }

            AudioSource newSource = Instantiate(audioSourcePrefab, this.transform);
            _audioSources.Add(newSource);
            return newSource;
        }
    }
}