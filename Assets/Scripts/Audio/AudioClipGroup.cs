using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [CreateAssetMenu(menuName = "AudioClipGroup")]
    public class AudioClipGroup : ScriptableObject
    {
        [Range(0, 2)] public float volumeMin = 1;
        [Range(0, 2)] public float volumeMax = 1;
        [Range(0, 2)] public float pitchMin = 1;
        [Range(0, 2)] public float pitchMax = 1;

        public float delay = 0.1f;
        public List<AudioClip> audioClips;

        private float _timestamp;

        public void OnEnable()
        {
            _timestamp = 0;
        }

        public void Play()
        {
            if (AudioSourcePool.Instance == null) return;

            Play(AudioSourcePool.Instance.GetAudioSource());
        }

        public void Play(AudioSource source)
        {
            if (audioClips.Count <= 0) return;
            if (_timestamp > Time.time) return;
            _timestamp = Time.time + delay;

            source.volume = Random.Range(volumeMin, volumeMax);
            source.pitch = Random.Range(pitchMin, pitchMax);
            source.clip = audioClips[Random.Range(0, audioClips.Count)];
            source.Play();
        }
    }
}