using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGB : MonoBehaviour
{
    public float colorChangeInterval = 2f;
    public float minColor = 0;
    public float maxColor = 100;
    private Light _light;

    private float _nextColorChange;
    // Start is called before the first frame update
    void Start()
    {
        _light = GetComponent<Light>();
        _nextColorChange = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!(_nextColorChange <= Time.time)) return;
        var r = Random.Range(minColor, maxColor);
        var g = Random.Range(minColor, maxColor);
        var b = Random.Range(minColor, maxColor);

        _light.color = new Color(r, g, b);

        _nextColorChange = Time.time + colorChangeInterval;
    }
}
