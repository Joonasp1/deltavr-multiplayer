using UnityEngine;

namespace Bow
{
    public class ArcheryTargetCollisionProxy : MonoBehaviour
    {
        public ArcheryTarget target;
    
        public void OnArrowHit(Arrow arrow)
        {
            if (arrow == null) return;
            if (target == null) return;

            target.OnArrowHit(arrow);
        }
    }
}