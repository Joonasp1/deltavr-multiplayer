﻿using Audio;
using UnityEngine.XR.Interaction.Toolkit;

namespace Bow
{
    public class Notch : XRSocketInteractor
    {
        public AudioClipGroup nockSounds;
        public AudioClipGroup releaseSounds;

        private Puller _puller = null;
        private Arrow _currentArrow = null;

        protected override void Awake()
        {
            base.Awake();
            _puller = GetComponent<Puller>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _puller.onSelectExited.AddListener(TryToReleaseArrow);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _puller.onSelectExited.RemoveListener(TryToReleaseArrow);
        }

        protected override void OnSelectEntered(XRBaseInteractable interactable)
        {
            base.OnSelectEntered(interactable);
            StoreArrow(interactable);
        }

        private void StoreArrow(XRBaseInteractable interactable)
        {
            if (!(interactable is Arrow arrow)) return;
            
            _currentArrow = arrow;
            nockSounds.Play();
        }

        private void TryToReleaseArrow(XRBaseInteractor interactor)
        {
            if (!_currentArrow) return;

            ForceDeselect();
            ReleaseArrow();
        }

        private void ForceDeselect()
        {
            _currentArrow.OnSelectExiting(this);
            _currentArrow.OnSelectExited(this);
            base.OnSelectExiting(_currentArrow);
            base.OnSelectExited(_currentArrow);
        }

        private void ReleaseArrow()
        {
            _currentArrow.Release(_puller.PullAmount);
            _currentArrow = null;
            releaseSounds.Play();
        }

        public override XRBaseInteractable.MovementType? selectedInteractableMovementTypeOverride =>
            XRBaseInteractable.MovementType.Instantaneous;
    }
}