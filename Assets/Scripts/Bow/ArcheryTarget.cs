using System;
using UnityEngine;
using Photon.Pun;
using Random = UnityEngine.Random;

namespace Bow
{
    public class ArcheryTarget : MonoBehaviourPun
    {
        public GameObject pointsText;
        public Vector3 endPosition;
        public float forwardSpeed = 2f;
        public float sidewaysSpeed = 1f;
        public ArcheryRange archeryRange;

        public Vector3 minRandomOffset = new Vector3();
        public Vector3 maxRandomOffset = new Vector3();

        private bool _flipDirection;

        private void Start()
        {
            _flipDirection = Random.value > 0.5f;

            var position = transform.position;

            minRandomOffset.z += position.z;
            maxRandomOffset.z += position.z;
        }

        // Update is called once per frame
        void Update()
        {
            float step = forwardSpeed * Time.deltaTime;
            var position = transform.position;

            if (Math.Abs(position.x - endPosition.x) < 0.1) Destroy(gameObject);

            if (Math.Abs(position.z - maxRandomOffset.z) < 0.1)
            {
                _flipDirection = true;
            }

            if (Math.Abs(position.z - minRandomOffset.z) < 0.1)
            {
                _flipDirection = false;
            }

            float desiredZ = _flipDirection ? maxRandomOffset.z : minRandomOffset.z;

            transform.position = Vector3.MoveTowards(position,
                new Vector3(endPosition.x, position.y, desiredZ), step);
        }

        [PunRPC]
        public void OnArrowHitPun()
        {
            var position = transform.position;

            float score = (float) Math.Round(Vector3.Distance(position, endPosition));
            archeryRange.AddScore(score);

            GameObject prefab = Instantiate(pointsText, position, Quaternion.Euler(0, 90f, 0), null);
            PointsText target = prefab.GetComponent<PointsText>();

            target.SetPoints(score);
            
        }

        public void OnArrowHit(Arrow arrow)
        {
            if (arrow == null) return;
            Destroy(arrow.gameObject);

            photonView.RPC("OnArrowHitPun", RpcTarget.All);
            PhotonNetwork.Destroy(photonView);
        }
    }
}