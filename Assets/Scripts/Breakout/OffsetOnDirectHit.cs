using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class OffsetOnDirectHit : MonoBehaviour
{
    public float minAngle = 0;
    public float maxAngle = 20;

    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = new Rigidbody();
    }

    private void OnCollisionEnter(Collision other)
    {
        Vector3 normal = other.contacts[0].normal;
        Vector3 velocity = _rigidbody.velocity;

        // Get Angle
        float angle = Vector3.Angle(velocity, -normal);

        if (angle > minAngle & angle < maxAngle)
        {
            // Add random torque to randomize direction
            _rigidbody.AddTorque(new Vector3(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f)));
        }
    }
}