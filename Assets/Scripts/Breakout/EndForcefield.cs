using UnityEngine;

namespace Breakout
{
    public class EndForcefield : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            BreakableCube breakableCube = other.gameObject.GetComponent<BreakableCube>();

            if (breakableCube != null)
            {
                Events.BreakoutEndGame();
            }
        }
    }
}
