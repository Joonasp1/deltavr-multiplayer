using System;
using UnityEngine;

namespace Breakout
{
    public class BreakoutBall : MonoBehaviour
    {
        public float speed = 2f;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            Events.OnBreakoutEndGame += DestroyBall;
        }

        private void OnDestroy()
        {
            Events.OnBreakoutEndGame -= DestroyBall;
        }

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            // Fix velocity on X axis so ball is more predictable. 
            var velocity = _rigidbody.velocity;
            velocity.x = Math.Sign(velocity.x) * speed;
            velocity = velocity.normalized * (speed * 1.5f);
            _rigidbody.velocity = velocity;
        }

        private void DestroyBall()
        {
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            KinematicSpeedTransfer kst = other.gameObject.GetComponent<KinematicSpeedTransfer>();

            if (kst == null) return;

            Events.BreakoutStartGame();
        }
    }
}