using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Breakout;
using Unity.Mathematics;
using UnityEngine;
using Photon.Pun;
using Random = UnityEngine.Random;

public class BreakoutManager : MonoBehaviourPunCallbacks
{
    public GameObject defaultBreakablePrefab;
    public GameObject extraBallBreakablePrefab;

    public GameObject startBallSpawnPoint;
    public GameObject ballPrefab; 
    
    public List<Transform> spawnPoints;
    
    public float timeBetweenRows = 5f;
    public float startRowSpeed = 0.15f;
    //private float _speedIncreasePerRow = 0.01f;
    public float extraBallBreakableChance = 0.1f;
    
    private float _nextRowTime;
    private float _currentRowSpeed;

    private int _ballCount;

    private bool _gameStarted;
    
    private void Awake()
    {
        _gameStarted = false;
        _ballCount = 1;
        _nextRowTime = Time.time;
        _currentRowSpeed = startRowSpeed;
        
        Events.OnBreakoutEndGame += EndGame;
        Events.OnBreakoutStartGame += StartGame;

        Events.OnBreakoutReduceBalls += ReduceBalls;
        Events.OnBreakoutAddBalls += AddBalls;
    }

    void OnDestroy()
    {
        Events.OnBreakoutEndGame -= EndGame;
        Events.OnBreakoutStartGame -= StartGame;

        Events.OnBreakoutReduceBalls -= ReduceBalls;
        Events.OnBreakoutAddBalls -= AddBalls;

    }

    void Update()
    {
        if (!_gameStarted) return;
        
        if (_nextRowTime < Time.time)
        {
            SpawnRow();
        }
    }

    void StartGame()
    {
        if (_gameStarted) return;
        _gameStarted = true;
        _nextRowTime = Time.time;
    }

    private void SpawnBall()
    {
        PhotonNetwork.Instantiate("Ball_Network", startBallSpawnPoint.transform.position, Quaternion.identity);
        //Instantiate(ballPrefab, startBallSpawnPoint.transform.position, Quaternion.identity, null);
        _ballCount += 1;
    }

    void EndGame()
    {
        if (!_gameStarted) return;
        _gameStarted = false;
       
        SpawnBall();
        _ballCount = 1;
    }

    void SpawnRow()
    {
        _nextRowTime = Time.time + timeBetweenRows;
        //currentRowSpeed += speedIncreasePerRow;

        foreach (var point in spawnPoints)
        {
            GameObject box;

            if (Random.value <= extraBallBreakableChance)
            {
                box = PhotonNetwork.Instantiate("Breakable Cube Extra Ball_Network", point.position, Quaternion.identity);
                //box = Instantiate(extraBallBreakablePrefab, point.position, Quaternion.identity, null);
            }
            else
            {
                box = PhotonNetwork.Instantiate("Breakable Cube_Network", point.position, Quaternion.identity);
                //box = Instantiate(defaultBreakablePrefab, point.position, Quaternion.identity, null);
            }

            var cube = box.GetComponent<BreakableCube>();

            cube.speed.x = _currentRowSpeed;
        }

        Events.BreakoutSetSpeed(_currentRowSpeed);

    }

    void ReduceBalls()
    {
        _ballCount -= 1;

        if (_ballCount <= 0)
        {
            Events.BreakoutEndGame();
        }
    }

    void AddBalls()
    {
        _ballCount += 1;
    }
}
