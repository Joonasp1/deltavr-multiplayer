using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] CharacterController controller;
    [SerializeField] float speed = 10f;

    Vector2 horizontalInput;

    private void Update()
    {

        Vector3 horizontalVelocity = (transform.right * horizontalInput.x + transform.forward * horizontalInput.y) * speed;
        controller.Move(horizontalVelocity * Time.deltaTime);
        controller.Move(new Vector3(0f, -9f, 0f));

        
    }

    public void ReceiveInput (Vector2 _horizontalInput)
    {
        horizontalInput = _horizontalInput;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Door")
        {
            Debug.Log("DOOR COLLISION");
            collision.gameObject.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0f, 0f, 20f));
        }
        
    }
}
