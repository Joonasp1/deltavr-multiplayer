using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MouseLook : MonoBehaviourPunCallbacks
{
    [SerializeField] float sensitivityX = 5f;
    [SerializeField] float sensitivityY = 5f;
    float mouseX, mouseY;

    [SerializeField] Transform playerCamera;
    [SerializeField] float xClamp = 85f;
    float xRotation = 0f;

    [SerializeField] GameObject plcamera;

    [SerializeField] LayerMask layerMask;

    private void Awake()
    {
        if(!photonView.IsMine)
        {
            plcamera.SetActive(false);
        }
        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {


        transform.Rotate(Vector3.up, mouseX * Time.deltaTime);

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -xClamp, xClamp);
        Vector3 targetRotation = transform.eulerAngles;
        targetRotation.x = xRotation;
        playerCamera.eulerAngles = targetRotation;


        RaycastHit hit;
        if (Physics.Raycast(playerCamera.position, transform.TransformDirection(Vector3.forward), out hit, 1f, layerMask))
        {
            Debug.LogWarning(hit.transform.name);
            if (hit.transform.gameObject.name == "VR-door")
            {
                GameObject door = hit.transform.gameObject;
                int direction = 0;
                if (playerCamera.position.z - door.transform.position.z < 0) direction = -1;
                else direction = 1;
                door.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * 10f);
            }
            
        }

    }

    public void Action()
    {
        Debug.Log("ACTION");
        RaycastHit actionHit;
        if (Physics.Raycast(playerCamera.position, transform.TransformDirection(Vector3.forward), out actionHit, 1f))
        {
            if(actionHit.transform.gameObject.tag == "Treasure")
            {
                Debug.Log("OBJECT FOUND");
                actionHit.transform.GetComponent<TreasureObject>().CollectedPun();
            }
        }
    }

    public void ReceiveInput (Vector2 mouseInput)
    {
        mouseX = mouseInput.x * sensitivityX;
        mouseY = mouseInput.y * sensitivityY;
    }

}
