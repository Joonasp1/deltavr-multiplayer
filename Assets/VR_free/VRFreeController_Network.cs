using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class VRFreeController_Network : MonoBehaviour
{
    private PhotonView photonView;
    public GameObject player;


    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (photonView.IsMine)
        {
            player.SetActive(false);
        }
    }

}
