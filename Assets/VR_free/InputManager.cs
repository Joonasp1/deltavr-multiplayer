using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class InputManager : MonoBehaviourPunCallbacks
{
    [SerializeField] Movement movement;
    [SerializeField] MouseLook mouseLook;
    [SerializeField] GameObject menu;

    VRFreeControls controls;
    VRFreeControls.MovementActions groundMovement;

    Vector2 horizontalInput;
    Vector2 mouseInput;

    private void Awake()
    {
        controls = new VRFreeControls();
        groundMovement = controls.Movement;

        groundMovement.HorizontalMovement.performed += ctx => horizontalInput = ctx.ReadValue<Vector2>();

        groundMovement.MouseX.performed += ctx => mouseInput.x = ctx.ReadValue<float>();
        groundMovement.MouseY.performed += ctx => mouseInput.y = ctx.ReadValue<float>();

        controls.Interactions.Action.performed += ctx => StartAction();

        controls.Menu.Menu.performed += ctx => OpenMenu();


        if (!photonView.IsMine)
        {
            controls.Disable();
        }

    }

    private void Update()
    {
        if (photonView.IsMine)
        {
            movement.ReceiveInput(horizontalInput);
            mouseLook.ReceiveInput(mouseInput);
        }
    }

    void OpenMenu()
    {
        menu.SetActive(!menu.activeInHierarchy);
    }

    void StartAction()
    {
        mouseLook.Action();
    }


    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
