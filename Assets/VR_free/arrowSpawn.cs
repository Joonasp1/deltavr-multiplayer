using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowSpawn : MonoBehaviour
{
    public float force = 1000f;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0f, 0f, force));
        StartCoroutine(Despawn());
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, this.GetComponent<Rigidbody>().velocity);
    }


    IEnumerator Despawn()
    {
        Debug.Log(this.GetComponent<Rigidbody>().velocity);
        yield return new WaitForSeconds(5f);
        Debug.Log(this.GetComponent<Rigidbody>().velocity);
        Destroy(this.gameObject);
    }

}
